#include <igloo/igloo.h>
#include "functions.h"

using namespace igloo;


Context(TestOrdinal) {
    Spec(Test1){
        Assert::That(makeOrdinal(1), Equals("1st"));
    }
    Spec(Test2){
        Assert::That(makeOrdinal(2), Equals("2nd"));
    }
    Spec(Test3){
        Assert::That(makeOrdinal(3), Equals("3rd"));
    }
    Spec(Test4){
        Assert::That(makeOrdinal(4), Equals("4th"));
    }
    Spec(Test5){
        Assert::That(makeOrdinal(5), Equals("5th"));
    }
    Spec(Test6){
        Assert::That(makeOrdinal(20), Equals("20th"));
    }
    Spec(Test7){
        Assert::That(makeOrdinal(21), Equals("21st"));
    }
    Spec(Test8){
        Assert::That(makeOrdinal(22), Equals("22nd"));
    }
    Spec(Test9){
        Assert::That(makeOrdinal(23), Equals("23rd"));
    }
    Spec(Test10){
        Assert::That(makeOrdinal(10), Equals("10th"));
    }
    Spec(Test11){
        Assert::That(makeOrdinal(11), Equals("11th"));
    }
    Spec(Test12){
        Assert::That(makeOrdinal(12), Equals("12th"));
    }
    Spec(Test13){
        Assert::That(makeOrdinal(13), Equals("13th"));
    }
    Spec(Test14){
        Assert::That(makeOrdinal(511), Equals("511th"));
    }
    Spec(Test15){
        Assert::That(makeOrdinal(712), Equals("712th"));
    }
    Spec(Test16){
        Assert::That(makeOrdinal(313), Equals("313th"));
    }

};


int main(int argc, const char* argv[]){
    TestRunner::RunAllTests(argc, argv);
}





