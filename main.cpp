#include <iostream>
#include "functions.h"

using namespace std;


int main(){

    // Ordinal Numbers

    // 1 : First : 1st

    // 88 -> 88th
    // 22 -> 22nd (not 22th)

    int x;
    cin >> x;

    string ordinal = makeOrdinal(x);

    cout << ordinal << endl;

    // cout << 13242547 % 100 << endl;

    return 0;
}