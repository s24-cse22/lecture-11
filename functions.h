#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <string>

std::string makeOrdinal(int number){

    // imagine number = 14 / 10

    int lastDigit = number % 10;
    int lastTwo = number % 100;

    if (lastTwo == 11 || lastTwo == 12 || lastTwo == 13){
        return std::to_string(number) + "th";
    }

    if (lastDigit == 1){
        return std::to_string(number) + "st";
    }
    else if (lastDigit == 2){
        return std::to_string(number) + "nd";
    }
    else if (lastDigit == 3){
        return std::to_string(number) + "rd";
    }
    else {
        return std::to_string(number) + "th";
    }
}

#endif